---
title: "About"
slug: "about"
date: 2012-02-27T14:27:35-03:00
thumbnail: "images/tn.png"
description: "about"
---

---------------------------
Although I have been programming for many years, I cannot call myself an expert and besides english is not my native language. In fact, one of the reason I write this blog is also to learn: I will really appreciate if you can correct my mistakes or misunderstandings. 

This blog is created with [Hugo](https://gohugo.io/) based on [hugo-coder-portfolio](https://github.com/naro143/hugo-coder-portfolio).   
The home image Logo credit goes to [wowauwero](http://wowauwero.deviantart.com/).  
It is hosted by [Netlify](https://www.netlify.com/)  and it's source code lives in [GitLab](https://about.gitlab.com/)


---------------------------



